﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public static SceneLoader scene;


    private void Awake()
    {
        if (scene == null)
        {
            DontDestroyOnLoad(gameObject);
            scene = this;
        }
        else if (scene != this)
        {
            Destroy(gameObject);
        }
    }

    public string GetActiveScene()
    {
        return SceneManager.GetActiveScene().name;
    }

    public void Load(string sceneName)
    {
        if (TimeManager.time)
        {
            TimeManager.time.ResetTimeScale();
        }
        SceneManager.LoadScene(sceneName);
    }

    public void LoadNextScene()
    {
        if (TimeManager.time)
        {
            TimeManager.time.ResetTimeScale();
        }
        Scene active = SceneManager.GetActiveScene();
        int index = (active.buildIndex + 1) % SceneManager.sceneCountInBuildSettings;
        SceneManager.LoadScene(index);
    }

    public void LoadPreviousScene()
    {
        if (TimeManager.time)
        {
            TimeManager.time.ResetTimeScale();
        }
        Scene active = SceneManager.GetActiveScene();
        int index = (active.buildIndex - 1) % SceneManager.sceneCountInBuildSettings;
        while (index < 0)
            index += SceneManager.sceneCountInBuildSettings;
        SceneManager.LoadScene(index);
    }

    public void Quit()
    {
        Application.Quit();
    }
}