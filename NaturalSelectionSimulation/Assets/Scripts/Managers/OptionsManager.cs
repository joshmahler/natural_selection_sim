﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionsManager : MonoBehaviour
{
    public static OptionsManager opt;

    [SerializeField] private Canvas menuCanvas;
    [SerializeField] private Canvas optionsCanvas;
    [SerializeField] private Text environmentText;
    [SerializeField] private Slider preySizeSlider;
    [SerializeField] private Slider predSizeSlider;
    [SerializeField] private Slider foodAmountSlider;
    [SerializeField] private Slider foodSpawnSlider;
    [SerializeField] private Slider foodRewardSlider;
    [SerializeField] private Slider foodStealSlider;
    [SerializeField] private Slider trialTimeSlider;
    [SerializeField] private Slider minSeekRadiusSlider;
    [SerializeField] private Slider maxSeekRadiusSlider;
    [SerializeField] private Slider minSpeedSlider;
    [SerializeField] private Slider maxSpeedSlider;

    private int defaultPreySize = 50;
    private int defaultPredSize = 10;
    private int defaultFoodAmount = 50;
    private float defaultFoodSpawn = 0.5f;
    private float defaultFoodReward = 1f;
    private float defaultFoodSteal = 0.3f;
    private float defaultTrialTime = 5f;
    private float defaultMinSeekRadius = 1f;
    private float defaultMaxSeekRadius = 5f;
    private float defaultMinSpeed = 1f;
    private float defaultMaxSpeed = 2000f;

    private int environmentClicks = 0;

    public int PreySize
    {
        get;
        private set;
    }
    public int PredatorSize
    {
        get;
        private set;
    }
    public int FoodAmount
    {
        get;
        private set;
    }
    public float FoodSpawn
    {
        get;
        private set;
    }
    public float FoodReward
    {
        get;
        private set;
    }
    public float FoodSteal
    {
        get;
        private set;
    }
    public float TrialTime
    {
        get;
        private set;
    }

    public float MinSeekRadius
    {
        get;
        private set;
    }

    public float MaxSeekRadius
    {
        get;
        private set;
    }

    public float MinSpeed
    {
        get;
        private set;
    }

    public float MaxSpeed
    {
        get;
        private set;
    }

    private void Awake()
    {
        Application.targetFrameRate = 60;

        if (opt == null)
        {
            DontDestroyOnLoad(gameObject);
            opt = this;
        }
        else if (opt != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        PreySize =       defaultPreySize =      (int)preySizeSlider.value;
        PredatorSize =   defaultPredSize =      (int)predSizeSlider.value;
        FoodAmount =     defaultFoodAmount =    (int)foodAmountSlider.value;
        FoodSpawn =      defaultFoodSpawn =     foodSpawnSlider.value;
        FoodReward =     defaultFoodReward =    foodRewardSlider.value;
        FoodSteal =      defaultFoodSteal =     foodStealSlider.value;
        TrialTime =      defaultTrialTime =     trialTimeSlider.value;
        MinSeekRadius =  defaultMinSeekRadius = minSeekRadiusSlider.value;
        MaxSeekRadius  = defaultMaxSeekRadius = maxSeekRadiusSlider.value;
        MinSpeed =       defaultMinSpeed =      minSpeedSlider.value;
        MaxSpeed =       defaultMaxSpeed =      maxSpeedSlider.value;
    }

    private void Update()
    {
        if(SceneManager.GetActiveScene().name == "MenuScene")
        {
            if(!menuCanvas)
                menuCanvas = GameObject.Find("MenuButtonCanvas").GetComponent<Canvas>();
        }
        else if(optionsCanvas.isActiveAndEnabled)
        {
            optionsCanvas.gameObject.SetActive(!optionsCanvas.isActiveAndEnabled);
        }
    }

    public void ToggleOptions()
    {
        menuCanvas.gameObject.SetActive(!menuCanvas.isActiveAndEnabled);
        optionsCanvas.gameObject.SetActive(!optionsCanvas.isActiveAndEnabled);
    }

    public void SaveValues()
    {
        PreySize = (int)preySizeSlider.value;
        PredatorSize = (int)predSizeSlider.value;
        FoodAmount = (int)foodAmountSlider.value;
        FoodSpawn = foodSpawnSlider.value;
        FoodReward = foodRewardSlider.value;
        FoodSteal = foodStealSlider.value;
        TrialTime = trialTimeSlider.value;
        MinSeekRadius = minSeekRadiusSlider.value;
        MaxSeekRadius = maxSeekRadiusSlider.value;
        MinSpeed = minSpeedSlider.value;
        MaxSpeed = maxSpeedSlider.value;
    }

    public void ResetValues()
    {
        preySizeSlider.value = PreySize = defaultPreySize;
        predSizeSlider.value = PredatorSize = defaultPredSize;
        foodAmountSlider.value = FoodAmount = defaultFoodAmount;
        foodSpawnSlider.value = FoodSpawn = defaultFoodSpawn;
        foodStealSlider.value = FoodSteal = defaultFoodSteal;
        foodRewardSlider.value = FoodReward = defaultFoodReward;
        trialTimeSlider.value = TrialTime = defaultTrialTime;
        minSeekRadiusSlider.value = MinSeekRadius = defaultMinSeekRadius;
        maxSeekRadiusSlider.value = MaxSeekRadius = defaultMaxSeekRadius;
        minSpeedSlider.value = MinSpeed = defaultMinSpeed;
        maxSpeedSlider.value = MaxSpeed = defaultMaxSpeed;
    }

    public void SetupEnvironment()
    {
        ++environmentClicks;
        environmentClicks = environmentClicks % 7;
        switch(environmentClicks)
        {
            case 0:
                //Normal
                environmentText.text = "Normal";
                ResetValues();
                break;
            case 1:
                //Tropical
                //Lots of food
                //Higher trial time
                //Much lower radius
                environmentText.text = "Tropical";
                preySizeSlider.value = PreySize = defaultPreySize;
                predSizeSlider.value = PredatorSize = defaultPredSize;
                foodAmountSlider.value = FoodAmount = defaultFoodAmount * 3;
                foodSpawnSlider.value = FoodSpawn = 0.01f;
                foodStealSlider.value = FoodSteal = defaultFoodSteal;
                foodRewardSlider.value = FoodReward = defaultFoodReward;
                trialTimeSlider.value = TrialTime = defaultTrialTime * 2;
                minSeekRadiusSlider.value = MinSeekRadius = defaultMinSeekRadius * 0.2f;
                maxSeekRadiusSlider.value = MaxSeekRadius = defaultMaxSeekRadius * 0.5f;
                minSpeedSlider.value = MinSpeed = defaultMinSpeed;
                maxSpeedSlider.value = MaxSpeed = defaultMaxSpeed;
                break;
            case 2:
                //Desert
                //Minimal food
                //Faster speed
                //Higher radius
                environmentText.text = "Desert";
                preySizeSlider.value = PreySize = defaultPreySize;
                predSizeSlider.value = PredatorSize = defaultPredSize;
                foodAmountSlider.value = FoodAmount = defaultFoodAmount / 2;
                foodSpawnSlider.value = FoodSpawn = defaultFoodSpawn * 2;
                foodStealSlider.value = FoodSteal = defaultFoodSteal;
                foodRewardSlider.value = FoodReward = defaultFoodReward;
                trialTimeSlider.value = TrialTime = defaultTrialTime;
                minSeekRadiusSlider.value = MinSeekRadius = defaultMinSeekRadius * 5;
                maxSeekRadiusSlider.value = MaxSeekRadius = defaultMaxSeekRadius * 5;
                minSpeedSlider.value = MinSpeed = defaultMinSpeed * 5;
                maxSpeedSlider.value = MaxSpeed = defaultMaxSpeed * 5;
                break;
            case 3:
                //Tundra
                //Slower speed
                //Lower food
                //Much higher food steal
                //Higher trial time
                environmentText.text = "Tundra";
                preySizeSlider.value = PreySize = defaultPreySize;
                predSizeSlider.value = PredatorSize = defaultPredSize;
                foodAmountSlider.value = FoodAmount = defaultFoodAmount / 2;
                foodSpawnSlider.value = FoodSpawn = defaultFoodSpawn;
                foodStealSlider.value = FoodSteal = defaultFoodSteal * 2;
                foodRewardSlider.value = FoodReward = defaultFoodReward * 3;
                trialTimeSlider.value = TrialTime = defaultTrialTime * 1.5f;
                minSeekRadiusSlider.value = MinSeekRadius = defaultMinSeekRadius;
                maxSeekRadiusSlider.value = MaxSeekRadius = defaultMaxSeekRadius;
                minSpeedSlider.value = MinSpeed = defaultMinSpeed * 0.5f;
                maxSpeedSlider.value = MaxSpeed = defaultMaxSpeed * 0.7f;
                break;
            case 4:
                //Jungle
                //Higher speed
                //Much lower min radius
                //Much higher max radius
                //High food amount/spawn
                //High prey/pred count
                environmentText.text = "Jungle";
                preySizeSlider.value = PreySize = defaultPreySize * 2;
                predSizeSlider.value = PredatorSize = defaultPredSize * 2;
                foodAmountSlider.value = FoodAmount = defaultFoodAmount * 3;
                foodSpawnSlider.value = FoodSpawn = 0.01f;
                foodStealSlider.value = FoodSteal = defaultFoodSteal;
                foodRewardSlider.value = FoodReward = defaultFoodReward;
                trialTimeSlider.value = TrialTime = defaultTrialTime;
                minSeekRadiusSlider.value = MinSeekRadius = defaultMinSeekRadius * 0.2f;
                maxSeekRadiusSlider.value = MaxSeekRadius = defaultMaxSeekRadius * 5;
                minSpeedSlider.value = MinSpeed = defaultMinSpeed * 4;
                maxSpeedSlider.value = MaxSpeed = defaultMaxSpeed * 4;
                break;
            case 5:
                //City
                //Slightly higher food
                //Lower radius
                //Higher trial time
                //Food reward lower
                //Lower speed
                environmentText.text = "City";
                preySizeSlider.value = PreySize = defaultPreySize;
                predSizeSlider.value = PredatorSize = defaultPredSize;
                foodAmountSlider.value = FoodAmount = defaultFoodAmount * 2;
                foodSpawnSlider.value = FoodSpawn = defaultFoodSpawn;
                foodStealSlider.value = FoodSteal = defaultFoodSteal * 0.3f;
                foodRewardSlider.value = FoodReward = defaultFoodReward;
                trialTimeSlider.value = TrialTime = defaultTrialTime * 3;
                minSeekRadiusSlider.value = MinSeekRadius = defaultMinSeekRadius * 0.4f;
                maxSeekRadiusSlider.value = MaxSeekRadius = defaultMaxSeekRadius * 0.4f;
                minSpeedSlider.value = MinSpeed = defaultMinSpeed * 0.3f;
                maxSpeedSlider.value = MaxSpeed = defaultMaxSpeed * 0.3f;
                break;
            case 6:
                //Zoo
                //High food
                //Low pred size
                //High prey size
                //Minimal food steal
                //Max food spawn
                //Low speed
                //Low radius
                environmentText.text = "Zoo";
                preySizeSlider.value = PreySize = defaultPreySize * 3;
                predSizeSlider.value = PredatorSize = defaultPredSize / 5;
                foodAmountSlider.value = FoodAmount = defaultFoodAmount * 3;
                foodSpawnSlider.value = FoodSpawn = 0.01f;
                foodStealSlider.value = FoodSteal = defaultFoodSteal * 0.1f;
                foodRewardSlider.value = FoodReward = defaultFoodReward;
                trialTimeSlider.value = TrialTime = defaultTrialTime;
                minSeekRadiusSlider.value = MinSeekRadius = defaultMinSeekRadius;
                maxSeekRadiusSlider.value = MaxSeekRadius = defaultMaxSeekRadius;
                minSpeedSlider.value = MinSpeed = defaultMinSpeed * 0.2f;
                maxSpeedSlider.value = MaxSpeed = defaultMaxSpeed * 0.2f;
                break;
        }
    }
}
