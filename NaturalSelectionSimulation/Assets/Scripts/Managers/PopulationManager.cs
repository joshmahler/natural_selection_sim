﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PopulationManager : MonoBehaviour
{
    [SerializeField] private static float elapsed = 0;
    [SerializeField] private static float elapsedFood = 0;

    [SerializeField] private GameObject preyPrefab;
    [SerializeField] private GameObject predatorPrefab;
    [SerializeField] private GameObject foodPrefab;
    [SerializeField] private List<WindowGraph> graphs;

    private List<GameObject> population = new List<GameObject>();
    private List<GameObject> foods = new List<GameObject>();
    private int generation = 1;
    private int spawnX = 20;
    private int spawnZ = 20;
    
    private Color startPreyColor = Color.green;
    private Color endPreyColor = Color.blue;
    private Color startPredatorColor = Color.black;
    private Color endPredatorColor = Color.red;

    private int numberName = 0;
    private int predatorCount = 0;
    private int preyCount = 0;

    GUIStyle guiStyle = new GUIStyle();
    private void OnGUI()
    {
        guiStyle.fontSize = 25;
        guiStyle.normal.textColor = Color.blue;
        GUI.BeginGroup(new Rect(10, 10, 250, 150));
        GUI.Box(new Rect(0, 0, 140, 140), "Stats", guiStyle);
        GUI.Label(new Rect(10, 25, 200, 30), "Gen: " + generation, guiStyle);
        GUI.Label(new Rect(10, 50, 200, 30), string.Format("Time: {0:0.00}", elapsed), guiStyle);
        GUI.Label(new Rect(10, 75, 200, 30), "Population: " + population.Count, guiStyle);
        GUI.Label(new Rect(10, 100, 200, 30), "Predator: " + predatorCount, guiStyle);
        GUI.Label(new Rect(10, 125, 200, 30), "Prey: " + preyCount, guiStyle);
        GUI.EndGroup();
    }

    private void Start()
    {
        for (int i = 0; i < OptionsManager.opt.PreySize; ++i)
        {
            GameObject prey = SpawnNewCreature(preyPrefab);
            prey.GetComponent<Brain>().Init(false);
            ChangeColor(prey, startPreyColor, endPreyColor);
            population.Add(prey);
        }
        for(int i = 0; i < OptionsManager.opt.PredatorSize; ++i)
        {
            GameObject pred = SpawnNewCreature(predatorPrefab);
            pred.GetComponent<Brain>().Init(true);
            ChangeColor(pred, startPredatorColor, endPredatorColor);
            population.Add(pred);
        }
        graphs[0].ChangeColor(1, 1, 1);
        graphs[1].ChangeColor(0.5f, 0, 0);
        graphs[2].ChangeColor(0, 0.5f, 0.5f);
        ResetPredatorPreyCounts();
        ResetFood();
    }

    private void Update()
    {
        elapsed += Time.deltaTime;
        elapsedFood += Time.deltaTime;
        if(elapsed >= OptionsManager.opt.TrialTime)
        {
            BreedNewPopulation();
            ResetPredatorPreyCounts();
            //ResetFood();
            elapsed = 0;
        }
        if(elapsedFood >= OptionsManager.opt.FoodSpawn)
        {
            SpawnNewFood();
            elapsedFood = 0;
        }
    }

    public GameObject FoodWithinRadius(Vector3 pos, float radius)
    {
        float min = radius;
        int savedIndex = -1;
        for (int i = 0; i < foods.Count; ++i)
        {
            float distance = (pos - foods[i].transform.position).magnitude;
            if (distance < min)
            {
                min = distance;
                savedIndex = i;
            }
        }

        if(savedIndex != -1)
            return foods[savedIndex];
        return null;
    }

    public bool ContainsFood(GameObject food)
    {
        if (foods.Contains(food))
            return true;
        return false;
    }

    /// <summary>
    /// If contains in the list, destroy the object and remove it from list.
    /// </summary>
    /// <param name="food">GameObject of the prefab.</param>
    /// <returns>True if destroyed properly, false if not contained in list.</returns>
    public bool DestroyFood(GameObject food)
    {
        if(foods.Contains(food))
        {
            Destroy(food);
            foods.Remove(food);
            return true;
        }
        return false;
    }

    public bool DestroyPrey(GameObject prey)
    {
        if(population.Contains(prey))
        {
            Destroy(prey);
            population.Remove(prey);
            return true;
        }
        return false;
    }

    /// <summary>
    /// This function sets up values for the graph
    /// </summary>
    private void ResetPredatorPreyCounts()
    {
        predatorCount = 0;
        preyCount = 0;
        for (int i = 0; i < population.Count; ++i)
        {
            if (population[i].GetComponent<Brain>().IsPredator)
                ++predatorCount;
            else
                ++preyCount;
        }

        graphs[0].AddValue(population.Count);
        graphs[1].AddValue(predatorCount);
        graphs[2].AddValue(preyCount);
    }

    private void ResetFood()
    {
        for (int i = 0; i < foods.Count; ++i)
        {
            Destroy(foods[i]);
        }
        foods.Clear();

        //This will reset the food to be the number what it started as
        for (int i = 0; i < OptionsManager.opt.FoodAmount; ++i)
        {
            Vector3 startingFoodPos = new Vector3(transform.position.x + Random.Range(-spawnX, spawnX),
                                            transform.position.y,
                                            transform.position.z + Random.Range(-spawnZ, spawnZ));

            GameObject food = Instantiate(foodPrefab, startingFoodPos, transform.rotation);

            foods.Add(food);
        }
    }

    private GameObject SpawnNewCreature(GameObject prefab)
    {
        //Get a position to spawn
        Vector3 startingPos = new Vector3(transform.position.x + Random.Range(-spawnX, spawnX),
                                    transform.position.y,
                                    transform.position.z + Random.Range(-spawnZ, spawnZ));

        //Spawn the offspring object
        GameObject offspring = Instantiate(prefab, startingPos, transform.rotation);

        //Rename it for ease of finding later if need be
        offspring.name = "Bot: " + numberName;
        ++numberName;

        return offspring;
    }

    private GameObject SpawnNewFood()
    {
        //Get a position to spawn
        Vector3 startingPos = new Vector3(transform.position.x + Random.Range(-spawnX, spawnX),
                            transform.position.y,
                            transform.position.z + Random.Range(-spawnZ, spawnZ));

        //Instantiate the food object
        GameObject food = Instantiate(foodPrefab, startingPos, transform.rotation);

        //Add it to the list
        foods.Add(food);

        return food;
    }

    private void ChangeColor(GameObject offspring, Color start, Color end)
    {
        float radius = offspring.GetComponent<Brain>().dna.GetGene(DNA.RADIUS);
        float normalizeRadius = (radius - OptionsManager.opt.MinSeekRadius) / (OptionsManager.opt.MaxSeekRadius - OptionsManager.opt.MinSeekRadius);
        offspring.GetComponent<MeshRenderer>().material.color = Color.Lerp(start, end, normalizeRadius);
    }

    private GameObject Breed(GameObject mom, GameObject dad, bool isPredator)
    {
        GameObject offspring;
        //Spawn a new creature depending on if its a predator or prey
        if (!isPredator)
            offspring = SpawnNewCreature(preyPrefab);
        else
            offspring = SpawnNewCreature(predatorPrefab);

        Brain brain = offspring.GetComponent<Brain>();
        if(Random.Range(0, 100) == 1) //Mutate 1 in 100
        {
            brain.Init(isPredator);
            brain.dna.Mutate();
        }
        else
        {
            //Combine dna from mom and dad
            brain.Init(isPredator);
            brain.dna.Combine(mom.GetComponent<Brain>().dna, dad.GetComponent<Brain>().dna);
        }

        //Change color based on predator or prey
        if (!isPredator)
        {
            ChangeColor(offspring, startPreyColor, endPreyColor);
        }
        else
        {
            ChangeColor(offspring, startPredatorColor, endPredatorColor);
        }

        return offspring;
    }

    private void BreedNewPopulation()
    {
        //Sort the list by Reward gene, from smallest to largest
        //This allows us to just split the list in half and take the more "fit"
        List<GameObject> sortedList = population.OrderBy(o => o.GetComponent<Brain>().dna.GetGene(DNA.REWARD)).ToList();
                
        for (int i = 0; i < sortedList.Count; ++i)
        {
            Brain brain = sortedList[i].GetComponent<Brain>();
            if (brain.dna.GetGene(DNA.REWARD) <= 0)
            {
                //Got no food, so it dies
                Destroy(sortedList[i]);
                population.Remove(sortedList[i]);
                sortedList.RemoveAt(i);
                --i;
            }
            else if (brain.dna.GetGene(DNA.REWARD) > 0 && brain.dna.GetGene(DNA.REWARD) <= 2)
            {
                //Remove from list so it doesn't reproduce
                //But don't destroy it bc it lives to the next day
                brain.dna.SetFloat(DNA.REWARD, 0);
                sortedList.RemoveAt(i);
                --i;
            }
            else
            {
                //By now, the list is filled with only creatures above 2 food
                brain.dna.SetFloat(DNA.REWARD, 0);
            }
        }

        if(sortedList.Count == 1)
        {
            Brain brain = sortedList[0].GetComponent<Brain>();
            population.Add(Breed(sortedList[0], sortedList[0], brain.IsPredator));
        }
        else
        {
            //Breed every creature still in the sorted list
            for (int i = 0; i < sortedList.Count - 1; ++i)
            {
                Brain brain = sortedList[i].GetComponent<Brain>();
                population.Add(Breed(sortedList[i], sortedList[i + 1], brain.IsPredator));
            }
        }

        ++generation;
    }
}
