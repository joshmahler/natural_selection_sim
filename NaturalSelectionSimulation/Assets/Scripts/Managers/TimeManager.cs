﻿using UnityEngine;
using System;

public class TimeManager : MonoBehaviour
{
    public static TimeManager time;

    [SerializeField] private string fasterKey;
    [SerializeField] private string slowerKey;
    [SerializeField] private string resetKey;
    
    private KeyCode faster = KeyCode.M;
    private KeyCode slower = KeyCode.N;
    private KeyCode reset = KeyCode.R;
    private float amount = 0.1f;
    private float savedTimeScale = 1f;
    private GameObject pauseCanvas;
    private bool isPaused = false;
    private bool didFind = false;

    private void Awake()
    {
        if (time == null)
        {
            DontDestroyOnLoad(gameObject);
            time = this;
        }
        else if (time != this)
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        PlayerPrefs.SetString("Fast", fasterKey.ToUpper());
        PlayerPrefs.SetString("Slow", slowerKey.ToUpper());
        PlayerPrefs.SetString("Reset", resetKey.ToUpper());

        if (PlayerPrefs.HasKey("Fast"))
            faster = (KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Fast"));
        if (PlayerPrefs.HasKey("Slow"))
            slower = (KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Slow"));
        if (PlayerPrefs.HasKey("Reset"))
            reset = (KeyCode)Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Reset"));
    }

    private void Update()
    {
        Init();
        if (didFind)
        {
            if (!isPaused)
                ModifyTimeScale();
            CheckPause();
        }
    }

    private void Init()
    {
        if (!didFind && SceneLoader.scene.GetActiveScene() == "SimulationScene")
        {
            pauseCanvas = GameObject.Find("PauseMenuCanvas");
            if (pauseCanvas)
            {
                pauseCanvas.GetComponent<Canvas>().enabled = true;
                pauseCanvas.SetActive(false);
                didFind = true;
            }
        }
        else if (SceneLoader.scene.GetActiveScene() != "SimulationScene")
        {
            didFind = false;
        }
    }

    public bool GetPaused()
    {
        return isPaused;
    }

    public void ResetTimeScale()
    {
        Time.timeScale = 1f;
        savedTimeScale = 1f;
        isPaused = false;
    }

    private void ModifyTimeScale()
    {
        if (Input.GetKey(slower) && Time.timeScale > amount)
        {
            Time.timeScale -= amount;
        }
        else if (Input.GetKey(faster))
        {
            Time.timeScale += amount;
        }
        else if (Input.GetKeyUp(reset))
        {
            Time.timeScale = 1f;
        }
    }

    private void CheckPause()
    {
        if(!isPaused && Input.GetKeyUp(KeyCode.Escape))
        {
            pauseCanvas.SetActive(true);
            savedTimeScale = Time.timeScale;
            Time.timeScale = 0f;
            isPaused = true;
        }
        else if(isPaused && Input.GetKeyUp(KeyCode.Escape))
        {
            Unpause();
        }
    }

    public void Unpause()
    {
        pauseCanvas.SetActive(false);
        Time.timeScale = savedTimeScale;
        isPaused = false;
    }
}
