﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Movement : MonoBehaviour
{
    [SerializeField] private float gravity = 9.8f;

    [SerializeField] private float wanderCircleDistance = 5;
    [SerializeField] private float wanderCircleRadius = 3;

    private float distanceBuffer = 0.3f;
       
    private Rigidbody rb;
    private float inverseMass;
    private Vector3 target;
    private float wanderAngle;

    private Brain brain;
    private PopulationManager population;
    private TimeManager time;
    private GameObject foodSighted;
    private bool towardsFood = false;


    private void Start()
    {
        population = FindObjectOfType<PopulationManager>();
        time = FindObjectOfType<TimeManager>();
        brain = GetComponent<Brain>();
        rb = GetComponent<Rigidbody>();
        inverseMass = 1 / rb.mass;
        GetWanderTarget();
    }

    private void Update()
    {
        if (time && !time.GetPaused())
        {
            if (rb)
            {
                if (towardsFood)
                {
                    if (!population.ContainsFood(foodSighted))
                    {
                        GetWanderTarget();
                        towardsFood = false;
                    }
                    if (Distance(target) < distanceBuffer)
                    {
                        GetWanderTarget();
                    }
                    else
                    {
                        Move();
                    }
                }
                else
                {
                    if (Distance(target) < distanceBuffer)
                    {
                        GetWanderTarget();
                    }
                    else //towardsFood == false
                    {
                        GameObject obj = population.FoodWithinRadius(transform.position, brain.dna.GetGene(DNA.RADIUS));
                        if (obj != null)
                        {
                            foodSighted = obj;
                            target = obj.transform.position;
                            towardsFood = true;
                        }
                        Move();
                    }
                }
            }
        }
    }

    private void SetAngle()
    {
        wanderAngle = Random.Range(0, 359);
    }

    private float Distance(Vector3 target)
    {
        Vector2 newTarget = new Vector2(target.x, target.z);
        Vector2 newPos = new Vector2(transform.position.x, transform.position.z);
        return (newTarget - newPos).magnitude;
    }

    private void GetWanderTarget()
    {
        Vector3 steering = Wander();
        steering *= inverseMass;
        target = steering;
    }

    private Vector3 Wander()
    {
        SetAngle();

        //Calculate the center
        Vector3 circleCenter = transform.position;
        circleCenter = circleCenter.normalized;
        circleCenter *= wanderCircleDistance;

        //Calculate the displacement force
        Vector3 displacement = new Vector3(Random.Range(-1, 1), 0, Random.Range(-1, 1));
        displacement.x = Mathf.Cos(wanderAngle) * displacement.magnitude;
        displacement.z = Mathf.Sin(wanderAngle) * displacement.magnitude;
        displacement *= wanderCircleRadius;

        //Calculate wander force
        Vector3 wanderForce = circleCenter + displacement;
        return wanderForce;
    }

    private void Move()
    {
        Vector3 direction = target - transform.position;
        direction.y = -gravity;
        direction = direction.normalized;
        rb.velocity = direction * brain.dna.GetGene(DNA.SPEED) * Time.deltaTime / Time.timeScale;
    }
}
