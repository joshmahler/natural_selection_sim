﻿using System.Collections.Generic;
using UnityEngine;

public class DNA
{
    public const int REWARD = 0;
    public const int RADIUS = 1;
    public const int SPEED = 2;

    private List<float> genes = new List<float>();
    private int dnaLength = 0;

    public DNA(int length)
    {
        dnaLength = length;

        genes.Clear();

        /*  Trait Key (by index):
         * 0 = Reward
         * 1 = Seek Radius
         * 2 = Speed
         */

        //Set up genes with all the different traits
        for (int i = 0; i < dnaLength; ++i)
        {
            genes.Add(0);
            float radius = Random.Range(OptionsManager.opt.MinSeekRadius, OptionsManager.opt.MaxSeekRadius);
            genes.Add(radius);
            float speed = Random.Range(OptionsManager.opt.MinSpeed, OptionsManager.opt.MaxSpeed);
            genes.Add(speed);
        }
    }

    public void SetFloat(int index, float value)
    {
        genes[index] = value;
    }

    public void AddFloat(int index, float value)
    {
        genes[index] += value;
    }

    public void Combine(DNA mom, DNA dad)
    {
        //Combines mom and dad DNA by half and half
        for(int i = 0; i < dnaLength; ++i)
        {
            if(i < dnaLength * 0.5f)
            {
                float c = mom.genes[i];
                genes[i] = c;
            }
            else
            {
                float c = dad.genes[i];
                genes[i] = c;
            }
        }
    }

    public void Mutate()
    {
        //Mutating will randomize all values in genes (except Reward)
        genes[RADIUS] = Random.Range(OptionsManager.opt.MinSeekRadius, OptionsManager.opt.MaxSeekRadius);
        genes[SPEED] = Random.Range(OptionsManager.opt.MinSpeed, OptionsManager.opt.MaxSpeed);
    }

    public float GetGene(int index)
    {
        return genes[index];
    }
}
