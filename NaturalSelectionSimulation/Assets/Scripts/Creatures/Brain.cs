﻿using UnityEngine;

[RequireComponent(typeof(Movement))]
public class Brain : MonoBehaviour
{
    public float timeAlive;
    public DNA dna;

    private int DNALength = 1;

    public bool IsPredator
    {
        get;
        private set;
    }

    private bool isAlive = true;
    private bool isInit = false;

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Dead")
        {
            isAlive = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isInit && other.tag == "PredatorHitbox" && tag != "Predator")
        {
            //Get the other (predator) brain
            Brain brain = other.transform.parent.GetComponent<Brain>();

            //If prey has any food
            if (dna.GetGene(DNA.REWARD) > 0)
            {
                //Steal the food by FoodSteal metric
                brain.Reward(OptionsManager.opt.FoodSteal);
                Reward(-OptionsManager.opt.FoodSteal);
            }
        }
    }

    public void Init(bool isPredator)
    {
        //Init the brain with a DNA
        dna = new DNA(DNALength);
        timeAlive = 0;
        isAlive = true;
        IsPredator = isPredator;
        isInit = true;
        dna.SetFloat(DNA.REWARD, 0);
    }

    private void Update()
    {
        if (isAlive)
            timeAlive += Time.deltaTime;
    }

    public void Reward(float reward)
    {
        dna.AddFloat(DNA.REWARD, reward);
    }
}
