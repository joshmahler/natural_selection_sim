﻿using UnityEngine;

public class Food : MonoBehaviour
{
    private PopulationManager population;
    private bool isInit = false;

    private void Start()
    {
        population = FindObjectOfType<PopulationManager>();
        isInit = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isInit && other.tag == "Prey")
        {
            population.DestroyFood(gameObject);
            other.GetComponent<Brain>().Reward(OptionsManager.opt.FoodReward);
        }
    }
}
