﻿using UnityEngine;
using UnityEngine.UI;

public class SliderNumber : MonoBehaviour
{
    private Text text;
    private Slider slider;

    private void Start()
    {
        text = GetComponent<Text>();
        slider = transform.parent.GetChild(0).GetComponent<Slider>();
    }

    private void Update()
    {
        text.text = slider.value.ToString("0.##");
    }
}
