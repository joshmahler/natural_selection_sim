﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonSetup : MonoBehaviour
{
    public enum FunctionCall
    {
        INVALID = -1,
        LOAD_PREV,
        LOAD_NEXT,
        OPTIONS_TOGGLE,
        RESET_VALUES,
        SAVE,
        CONTINUE,
        QUIT,
        ENVIRONMENT
    }

    [SerializeField] private FunctionCall call;

    private Button button;

    private void Start()
    {
        button = GetComponent<Button>();

        switch (call)
        {
            case FunctionCall.LOAD_PREV:
                button.onClick.AddListener(SceneLoader.scene.LoadPreviousScene);
                break;
            case FunctionCall.LOAD_NEXT:
                button.onClick.AddListener(SceneLoader.scene.LoadNextScene);
                break;
            case FunctionCall.OPTIONS_TOGGLE:
                button.onClick.AddListener(OptionsManager.opt.ToggleOptions);
                break;
            case FunctionCall.RESET_VALUES:
                button.onClick.AddListener(OptionsManager.opt.ResetValues);
                break;
            case FunctionCall.SAVE:
                button.onClick.AddListener(OptionsManager.opt.SaveValues);
                break;
            case FunctionCall.CONTINUE:
                button.onClick.AddListener(TimeManager.time.Unpause);
                break;
            case FunctionCall.QUIT:
                button.onClick.AddListener(SceneLoader.scene.Quit);
                break;
            case FunctionCall.ENVIRONMENT:
                button.onClick.AddListener(OptionsManager.opt.SetupEnvironment);
                break;
        }
    }
}
