﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowGraph : MonoBehaviour
{
    [SerializeField] private Sprite circleSprite;
    
    private RectTransform graphContainer;
    private List<int> values = new List<int>();
    private Color graphColor;

    private void Awake()
    {
        graphContainer = GetComponent<RectTransform>();
    }

    public void AddValue(int value)
    {
        values.Add(value);
        DeleteGraph();
        ShowGraph(values);
    }

    public void ChangeColor(float r, float g, float b)
    {
        graphColor = new Color(r, g, b, 0.75f);
    }

    private GameObject CreateCircle(Vector2 anchor)
    {
        //Instantiate the dot
        GameObject circle = new GameObject("circle", typeof(Image));
        circle.transform.SetParent(graphContainer, false);

        //Set the color and sprite
        Image image = circle.GetComponent<Image>();
        image.sprite = circleSprite;
        image.color = graphColor;

        //Get the transform
        RectTransform circleTr = circle.GetComponent<RectTransform>();
        circleTr.anchoredPosition = anchor;
        //Set the size of the dot
        circleTr.sizeDelta = new Vector2(10, 10);
        circleTr.anchorMin = new Vector2(0, 0);
        circleTr.anchorMax = new Vector2(0, 0);

        return circle;
    }

    private void DeleteGraph()
    {
        foreach(Transform child in graphContainer)
        {
            Destroy(child.gameObject);
        }
    }

    private void ShowGraph(List<int> values)
    {
        //Get the height of the graph
        float graphHeight = graphContainer.sizeDelta.y;
        float yMax = 200f;
        float xSize = 15f;
        GameObject lastCircle = null;
        bool isMoving = false;

        //Start from the back in case anything gets removed
        for(int i = values.Count - 1; i >= 0; --i)
        {
            float xPos = xSize * i;
            if (xPos > graphContainer.rect.width)
            {
                //If the graph is full, remove the first dot
                //and slide the remaining dots over by one xSize
                values.RemoveAt(0);
                isMoving = true;
                xPos -= xSize;
                --i;
            }
            if(!isMoving)
            {
                xPos += xSize;
            }
            float yPos = (values[i] / yMax) * graphHeight;

            //Create a dot and connect it to the previous dot
            GameObject circle = CreateCircle(new Vector2(xPos, yPos));
            if(lastCircle != null)
            {
                CreateDotConnection(lastCircle.GetComponent<RectTransform>().anchoredPosition,
                    circle.GetComponent<RectTransform>().anchoredPosition);
            }
            lastCircle = circle;
        }
    }

    private void CreateDotConnection(Vector2 dotPosA, Vector2 dotPosB)
    {
        //Instantiate connection
        GameObject connection = new GameObject("dotConnection", typeof(Image));
        connection.transform.SetParent(graphContainer, false);
        connection.GetComponent<Image>().color = graphColor;

        //Get the transform
        RectTransform connectionTr = connection.GetComponent<RectTransform>();
        Vector2 direction = (dotPosB - dotPosA).normalized;

        //Get the distance between the two dot positions
        float distance = Vector2.Distance(dotPosA, dotPosB);
        connectionTr.anchorMin = new Vector2(0, 0);
        connectionTr.anchorMax = new Vector2(0, 0);
        //Set the size of the connection to be the distance
        connectionTr.sizeDelta = new Vector2(distance, 3);
        connectionTr.anchoredPosition = dotPosA + direction * distance * 0.5f;
        
        //Get the angle between the dots
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        if (angle < 0) 
            angle += 360;

        //Connect the connection to the dots
        connectionTr.localEulerAngles = new Vector3(0, 0, angle);
    }
}
